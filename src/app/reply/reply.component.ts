import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-reply',
  templateUrl: './reply.component.html',
  styleUrls: ['./reply.component.css']
})
export class ReplyComponent implements OnInit {
  
  private sub: any;
  item: Observable<any>;

  constructor(private api: ApiService,private db: AngularFireDatabase,) {
  }

  ngOnInit() {
    this.item = this.api.post;
  }
  
  saveReply(id, message){
    const itemRef = this.db.object('replies/' + id);
    itemRef.set({"id": id, "message": message});
  }


}
