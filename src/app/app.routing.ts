import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { PostComponent } from './post/post.component';
import { ReplyComponent } from './reply/reply.component';
 
export const AppRoutes: Routes = [
    { path: '', component: PostComponent },
    { path: 'reply/:id', component: ReplyComponent }
];
 
export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);