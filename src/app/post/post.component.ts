import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { ApiService } from '../api.service';
import {Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  posts: Observable<any>;
  
  constructor(db: AngularFireDatabase, private api: ApiService, private router: Router) {
    this.posts = db.list('posts').valueChanges();
  
  }

  ngOnInit() {
  }
  
  goToReply(item){
    this.api.post = item;
    this.router.navigate(['/reply',item.id]);
  }

}
